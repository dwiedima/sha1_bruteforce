# SHA 1 Bruteforce Diskrete Mathematik Übungsaufgabe 2

## Passwörter

    + System 1 (5aea476328379d3bff2204501bb57aa8b4268fac) = X42aO           --> UNSICHER
    + System 2 (d31d62ed0af022248e28fc0dc4a9580217987e55) = -unbekannt-     --> SICHER
    + System 3 (66ceeafde8453dda201978b2b497b9c85d4b6da5) = qwertzui        --> UNSICHER (bekannter Hashcode)

## Verwendeter Rechner:
    + Modellname:                       MacBook Pro
    + Modell-Identifizierung:	    MacBookPro11,4
    + Prozessortyp:	                    Intel Core i7
    + Prozessorgeschwindigkeit:	    2,2 GHz
    + Anzahl der Prozessoren:	    1
    + Gesamtanzahl der Kerne:	    4
    + L2-Cache (pro Kern):	            256 KB
    + L3-Cache:	                    6 MB
    + Hyper-Threading Technologie:	    Aktiviert
    + Speicher:	                    16 GB
    
## Anzahl möglicher Passwörter:
    + System 1: 916.132.832
    + System 2: 839.299.365.868.340.224
    + System 3: 853.058.371.851.163.296
    
## Dauer der Berechnung aufgrund der Rechnerdaten
Nach Rechenleistung sind beim Bruteforce Algorithmus ca. 600.000 Iterationen pro Sekunde möglich. 

    + System 1: geknackt nach                                   25,43 Minuten
    + System 2: theoretisch geknackt nach                       16.190.188 Tagen
    + System 3 theoretisch geknackt (ohne Passwortliste) nach   16.455.601 Tagen
    
## Terminal Ausgabe bei Berechnung des Passworts (siehe it/s)
![Scheme](https://i.imgur.com/LDmeKn3.png)

## Screenshot des genutzten RAMS beim berechnen
![Scheme](https://i.imgur.com/Rypviki.png)

## Ergebnis der Berechnung von System 1 (Fehlermeldung wurde im Code bereits behoben)
    Ergebnis von System 1 wurde bei 81% der Iterationen gefunden
![Scheme](https://i.imgur.com/FU5eAvY.png)

## Schlussfolgerung
    Aufgrund der intensiven Rechenleistung ist System 2 und 3 nicht via Bruteforce mit durchschnittlichen Rechnern knackbar.
    Falls es jedoch möglich wäre statt 600.000 Iterationen, 10 Tera Iterationen pro Sekunde durchzuführen, wäre System 2 und 3 
    innerhalb von ca. 25 Stunden geknackt.
    Siehe: https://asecuritysite.com/encryption/passes