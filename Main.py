import hashlib, itertools, timeit
from tqdm import tqdm

# https://asecuritysite.com/encryption/passes

system_1_sha = "5aea476328379d3bff2204501bb57aa8b4268fac"  # 5 CHARS PW = X42aO
system_2_sha = "d31d62ed0af022248e28fc0dc4a9580217987e55"  # 10 CHARS
system_3_sha = "66ceeafde8453dda201978b2b497b9c85d4b6da5"  # 5-10 CHARS PW = qwertzui

vocabulary = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789" # Vocabulary [a-z][A-Z][0-9]

system_1_amount_passwords = pow(len(vocabulary), 5)      # 62 characters, pw length of 5
system_2_amount_passwords = pow(len(vocabulary), 10)     # 62 characters, pw length of 10
system_3_amount_passwords = 0                            # 62 characters, pw length between 5 and 10
for x in range(5, 11):
    system_3_amount_passwords += pow(len(vocabulary), x)

with open('./10-million-password-list-top-1000000.txt') as myFile:    # save passwords to list
    password_list = []
    for line in myFile:
        password_list.append(line.strip())  # removes new line character

print("AMOUNT PASSWORDS SYSTEM_1: " + str(system_1_amount_passwords) + " TIME TO CRACK: " + str((system_1_amount_passwords/600000)) + " SECONDS OR: " + str((system_1_amount_passwords/600000)/86400) + " DAYS")
print("AMOUNT PASSWORDS SYSTEM_2: " + str(system_2_amount_passwords) + " TIME TO CRACK: " + str((system_2_amount_passwords/600000)) + " SECONDS OR: " + str((system_2_amount_passwords/600000)/86400) + " DAYS")
print("AMOUNT PASSWORDS SYSTEM_3: " + str(system_3_amount_passwords) + " TIME TO CRACK: " + str((system_3_amount_passwords/600000)) + " SECONDS OR: " + str((system_3_amount_passwords/600000)/86400) + " DAYS")


def check_with_password_list(password_list, sha_password):
    for password in password_list:
        if sha_password == dec_sha1(password):
            return password

    return None


def get_cartesian_passwords(voc, min, max):  # return cartesian product list
    if min <= max:
        my_list = []
        if min == max:  # password with given range
            my_list = ["".join(x) for x in itertools.product(voc, repeat=max)]
        else:  # password with dynamic range
            for y in range(min, max+1):  # iterate over min and max
                my_list.extend(["".join(x) for x in itertools.product(voc, repeat=y)])  # extend the list with list by repeat count
        print("GENERATED PASSWORD LIST")
        return my_list
    else:
        return


def dec_sha1(password):
    return hashlib.sha1(password).hexdigest()


def bruteforce(voc, min, max, sha1_password): # 45GB RAM verwendet
    start = timeit.default_timer()
    possible_password = check_with_password_list(password_list, sha1_password)
    found = False
    if possible_password is not None:
        print("FOUND PASSWORD BY LIST: "+possible_password)
    else:
        print("PASSWORD DOES NOT EXIST IN LIST. STARTING BRUTEFORCE")
        bruteforce_list = get_cartesian_passwords(voc, min, max)
        for password in tqdm(bruteforce_list):
            if sha1_password == dec_sha1(password):
                end = timeit.default_timer()
                print("FOUND PASSWORD BY BRUTEFORCE: "+password)
                print("TIME TOOK: " + str(end-start))
                found = True
                break
        if not found:
            print("PASSWORD NOT FOUND")


bruteforce(vocabulary, 5, 5, system_1_sha)